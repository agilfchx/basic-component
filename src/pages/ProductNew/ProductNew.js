import React from 'react';
import styles from './styles.scoped.css';
import Form from '../../components/elements/Form';

export default function ProductNew() {
  return (
    <main className={styles.root}>
      <div className={styles.header}>
        <h1 className={styles.title}>
          <span style={{ color: '#6366f1' }}>Products</span> / Add New Product
        </h1>
      </div>
      <Form />
    </main>
  );
}
