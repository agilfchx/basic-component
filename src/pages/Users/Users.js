import React from 'react';
import user from './user.json';
import Table from '../../components/elements/Table';
import Modal from '../../components/elements/Modal';

export default function Users() {
  const column = ['USERNAME', 'EMAIL', 'ACTION'];

  const rowsData = (i, idx) => {
    return (
      <tr className="px-2 border-b border-slate-100" key={idx}>
        <td className="p-2 text-[#1E293B]">
          <div className="flex items-center space-x-3">
            <img
              className="rounded-full w-16 h-16 mr-3"
              src={i.profilePicture}
            />
            {i.username}
          </div>
        </td>
        <td className="p-2 text-[#475569]">{i.email}</td>
        <td className="p-2 pl-11">
          <Modal name={i.username} />
        </td>
      </tr>
    );
  };

  return (
    <div className="h-full overflow-auto shadow-lg bg-white">
      <p className="text-[#1E293B] text-lg font-semibold p-6">User</p>
      <div className="border-b" />
      <div className="px-5 pt-5">
        <Table columns={column} items={user} rows={rowsData} />
      </div>
    </div>
  );
}
