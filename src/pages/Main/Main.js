import React from 'react';

export default function Main() {
  return (
    <main>
      <div className="flex justify-center items-center">
        <h1 className="text-red-600 text-3xl">Hello World</h1>
      </div>
    </main>
  );
}
