import React, { useState, useEffect } from 'react';
// eslint-disable-next-line import/extensions
import initData from './product.json';
import Table from '../../components/elements/Table';
import { Link, useLocation } from 'react-router-dom';
import Modal from '../../components/elements/Modal';
import Button from '../../components/elements/Button';
import Checkbox from '../../components/elements/Checkbox';

export default function Products() {
  const { state } = useLocation();
  const [data, setData] = useState(initData);
  const [category, setCategory] = useState('');
  const [isChecked, setIsChecked] = useState(false);

  useEffect(() => {
    if (state !== null) {
      setData([state, ...data]);
    }
  }, [state]);

  const handleSelect = (e) => {
    setCategory(e.target.value);
    if (!e.target.value && !isChecked) {
      // console.log('no category and no check');
      setData(initData);
    } else if (!e.target.value && isChecked) {
      // console.log('no category and yes check');
      setData(initData.filter((i) => compareDate(i) || i.expiryDate === null));
    } else if (e.target.value && !isChecked) {
      // console.log('yes category and no check');
      setData(initData.filter((i) => i.category === e.target.value));
    } else if (isChecked && e.target.value) {
      // console.log('yes category and yes check');
      setData(
        initData.filter(
          (i) =>
            (compareDate(i) || i.expiryDate === null) &&
            i.category === e.target.value
        )
      );
    } else {
      setData(initData);
    }
  };

  const handleCheck = (e) => {
    setIsChecked(e.target.checked);
    if (isChecked && category === '') {
      // console.log('no check no category');
      setData(initData);
    } else if (!isChecked && category !== '') {
      // console.log('yes check and yes category');
      setData(
        initData.filter(
          (i) =>
            (compareDate(i) || i.expiryDate === null) && i.category === category
        )
      );
    } else if (isChecked && category !== '') {
      // console.log('no check and yes category');
      setData(initData.filter((i) => i.category === category));
    } else {
      // console.log('yes check and no category');
      setData(initData.filter((i) => compareDate(i) || i.expiryDate === null));
    }
  };

  const onDelete = (id) => {
    const newData = data.map((i) => {
      if (i.id === id) {
        i.isDeleted = true;
      }
      return i;
    });
    setData(newData);
    // console.log(newData);
  };

  const column = [
    'Product Name',
    'Description',
    'Product Price',
    'Category',
    'Expiry Date',
    'ACTION',
  ];

  const compareDate = (i) => {
    return new Date(i.expiryDate) > new Date();
  };

  const rowsData = (i, idx) => {
    return (
      !i.isDeleted && (
        <tr
          className={`border-b border-slate-100 w-full whitespace-pre-wrap ${
            compareDate(i) || i.expiryDate === null ? '' : 'opacity-parent'
          }`}
          key={idx}
        >
          <td className="p-3">
            <Link to={`/products/${i.id}`}>
              <div className="flex items-center space-x-3">
                <img
                  className={`rounded-full w-16 h-16 mr-3 ${
                    compareDate(i) || i.expiryDate === null ? '' : 'img-opac'
                  }`}
                  src={i.image}
                />
                {i.name}
              </div>
            </Link>
          </td>
          <td className="p-3">{i.description}</td>
          <td
            className={`p-3 text-center ${
              compareDate(i) || i.expiryDate === null ? 'green' : 'green-opac'
            }`}
          >
            Rp{numberWithDot(i.price)}
          </td>
          <td className="p-3">{i.category}</td>
          <td className="p-3">{i.expiryDate}</td>
          <td
            className={`p-3 text-center ${
              compareDate(i) || i.expiryDate === null ? '' : 'opacity-child'
            }`}
          >
            <Modal name={i.name} onDelete={() => onDelete(i.id)} />
          </td>
        </tr>
      )
    );
  };

  const numberWithDot = (number) => {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  };

  return (
    <div className="h-full overflow-auto shadow-lg bg-white">
      <div className="header-products">
        <h1 className="header-products__title">Products</h1>
        <div className="flex items-center">
          <Checkbox
            id="expired"
            labelText="Hide expired product"
            name="expired"
            onChange={handleCheck}
          />
          <select
            className="select-category"
            onChange={handleSelect}
            value={category}
          >
            <option value="">All Category</option>
            <option value="Ready">Ready</option>
            <option value="Barang Bekas">Barang Bekas</option>
            <option value="Pre-Order">Pre-Order</option>
          </select>
          <Link to="/products/new">
            <Button className="blue" text="Add Product" />
          </Link>
        </div>
      </div>
      <div className="border-b" />
      <div className="px-5 pt-5">
        <Table columns={column} items={data} rows={rowsData} />
      </div>
    </div>
  );
}
