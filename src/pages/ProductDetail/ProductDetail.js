import React from 'react';
import { useParams } from 'react-router-dom';

export default function ProductDetail() {
  const params = useParams();
  return (
    <main>
      <div className="flex justify-center px-12 py-2">
        <h1 className="text-center text-red-500 text-4xl">
          Detail Product {params.id}
        </h1>
      </div>
    </main>
  );
}
