import React from 'react';
import PropTypes from 'prop-types';
import { hot } from 'react-hot-loader/root';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { Provider } from 'react-redux';
import ErrorBoundary from './components/elements/ErrorBoundary';
import { Main, Products, Users, ProductDetail, ProductNew } from './pages';
import Layouts from './components/elements/Layouts/Layout';

const App = ({ store }) => {
  return (
    <ErrorBoundary>
      <Provider store={store}>
        <BrowserRouter>
          <Layouts>
            <Routes>
              <Route element={<Main />} exact path="/" />
              <Route element={<Products />} exact path="/products" />
              <Route element={<ProductNew />} exact path="/products/new" />
              <Route element={<ProductDetail />} exact path="/products/:id" />
              <Route element={<Users />} exact path="/users" />
            </Routes>
          </Layouts>
        </BrowserRouter>
      </Provider>
    </ErrorBoundary>
  );
};

export default hot(App);

App.propTypes = {
  store: PropTypes.object.isRequired,
};
