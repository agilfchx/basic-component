import React from 'react';
import styles from './styles.scoped.css';
import PropTypes from 'prop-types';

export default function Select({ children, id, name, option, onChange }) {
  return (
    <div className={styles.select}>
      {children}
      <select
        className={styles.select_box}
        defaultValue={option}
        id={id}
        name={name}
        onChange={onChange}
      >
        <option disabled>{option}</option>
        <option value="Ready">Ready</option>
        <option value="Barang Bekas">Barang Bekas</option>
        <option value="Pre-Order">Pre-Order</option>
      </select>
    </div>
  );
}

Select.defaultProps = {
  children: null,
};

Select.propTypes = {
  children: PropTypes.node,
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  option: PropTypes.string.isRequired,
};
