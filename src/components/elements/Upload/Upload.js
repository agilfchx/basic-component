import React from 'react';
import styles from './styles.scoped.css';
import PropTypes from 'prop-types';

export default function Upload({ onChange }) {
  return (
    <label className={styles.button} htmlFor="image">
      Upload Image
      <input
        accept="image/*"
        hidden
        id="image"
        name="image"
        onChange={onChange}
        type="file"
      />
    </label>
  );
}

Upload.propTypes = {
  onChange: PropTypes.func.isRequired,
};
