import React from 'react';
import styles from './styles.scoped.css';
import PropTypes from 'prop-types';

export default function Input({
  labelText,
  id,
  name,
  placeholder,
  type,
  onChange,
}) {
  return (
    <div className={styles.input}>
      <label htmlFor={name}>{labelText}</label>
      <input
        className={styles.input_box}
        id={id}
        name={name}
        onChange={onChange}
        placeholder={placeholder}
        required
        type={type}
      />
    </div>
  );
}

Input.defaultProps = {
  labelText: '',
};

Input.propTypes = {
  id: PropTypes.string.isRequired,
  labelText: PropTypes.string,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
};
