import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash, faX } from '@fortawesome/free-solid-svg-icons';

export default function Modal({ name, onDelete }) {
  const [showModal, setShowModal] = useState(false);

  return (
    <>
      <button>
        <FontAwesomeIcon icon={faTrash} onClick={() => setShowModal(true)} />
      </button>
      {showModal ? (
        <>
          <div className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none bg-gray-500 bg-opacity-40">
            <div className="relative p-8 bg-slate-100 w-full max-w-md m-auto flex-col flex rounded-lg shadow-lg border border-slate-300">
              <span className="absolute top-0 right-0 p-4">
                <button onClick={() => setShowModal(false)}>
                  <FontAwesomeIcon icon={faX} />
                </button>
              </span>
              <div className="text-left">
                <h2 className="text-xl">Konfirmasi Delete</h2>
                <div className="py-5 text-sm">
                  Hapus <span className="text-red-500"> {name} </span>?
                </div>
              </div>
              <div className="flex justify-end space-x-3">
                <button
                  className="p-3 w-24 bg-white hover:bg-slate-200 rounded-md border border-gray-300"
                  onClick={() => setShowModal(false)}
                >
                  Tidak
                </button>
                <button
                  className="p-3 w-24 bg-red-500 hover:bg-red-600 rounded-md text-white border border-transparent"
                  onClick={() => {
                    setShowModal(false);
                    onDelete();
                  }}
                >
                  Ya
                </button>
              </div>
            </div>
          </div>
        </>
      ) : null}
    </>
  );
}

Modal.defaultProps = {
  onDelete: () => {},
};

Modal.propTypes = {
  name: PropTypes.string.isRequired,
  onDelete: PropTypes.func,
};
