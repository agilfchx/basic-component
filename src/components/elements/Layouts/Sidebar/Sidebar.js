import React from 'react';
import Logo from '../../../../assets/Logo.svg';
import UsersIcon from '../../../../assets/Users.svg';
import ProductsIcon from '../../../../assets/Products.svg';
import { NavLink, Link } from 'react-router-dom';

export default function Sidebar() {
  return (
    <aside className="w-72 h-full bg-[#1E293B]">
      <div className="overflow-y-auto py-4 px-5 mb-1">
        <ul>
          <li className="mb-10">
            <Link to="/">
              <img height={40} src={Logo} width={40} />
            </Link>
          </li>
          <li>
            <p className="text-md text-[#64748B]">PAGES</p>
          </li>
        </ul>
      </div>
      <div className="px-3">
        <ul>
          <li>
            <NavLink
              className={({ isActive }) =>
                isActive ? 'highlighted-sidebar' : 'normal-sidebar'
              }
              to="/products"
            >
              <img className="ml-2" height={30} src={ProductsIcon} width={30} />
              <p>Products</p>
            </NavLink>
          </li>
          <li>
            <NavLink
              className={({ isActive }) =>
                isActive ? 'highlighted-sidebar' : 'normal-sidebar'
              }
              to="/users"
            >
              <img className="ml-2" height={30} src={UsersIcon} width={30} />
              <p>Users</p>
            </NavLink>
          </li>
        </ul>
      </div>
    </aside>
  );
}
