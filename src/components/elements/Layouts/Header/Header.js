import React from 'react';
import SearchIcon from '../../../../assets/Search.svg';
import MessageIcon from '../../../../assets/Message.svg';
import WarningIcon from '../../../../assets/Warning.svg';
import UserLogo from '../../../../assets/UserLogo.svg';
import ArrowDownIcon from '../../../../assets/ArrowDown.svg';

export default function Header() {
  return (
    <nav className="bg-white p-4 text-black flex justify-end relative">
      <ul className="flex space-x-3">
        <li className="rounded-icon">
          <img height={22} src={SearchIcon} width={22} />
        </li>
        <li className="rounded-icon">
          <img height={22} src={MessageIcon} width={22} />
        </li>
        <li className="rounded-icon">
          <img height={22} src={WarningIcon} width={22} />
        </li>
        <li className="w-0.5 bg-[#E2E8F0]" />
        <li className="rounded-full w-10 h-10 bg-[#334155] flex justify-center items-center mr-3">
          <img className="ml-1" height={20} src={UserLogo} width={20} />
        </li>
        <li className="flex items-center">
          <h1 className="text-xl text-[#475569] mr-5">Acne</h1>
          <img className="mr-8" height={15} src={ArrowDownIcon} width={15} />
        </li>
      </ul>
    </nav>
  );
}
