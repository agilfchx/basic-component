import React from 'react';
import Header from './Header/Header';
import Sidebar from './Sidebar';
import PropTypes from 'prop-types';

export default function Layout({ children }) {
  return (
    <div className="flex h-screen overflow-hidden">
      <Sidebar />
      <div className="flex flex-col w-full">
        <Header />
        <div className="p-8 pb-24 h-full">{children}</div>
      </div>
    </div>
  );
}

Layout.propTypes = {
  children: PropTypes.object.isRequired,
};
