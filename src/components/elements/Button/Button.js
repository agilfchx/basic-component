import React from 'react';
import './styles.css';
import PropTypes from 'prop-types';

export default function Button({ text, className, onClick }) {
  return (
    <button className={`btn btn-${className}`} onClick={onClick}>
      {text}
    </button>
  );
}

Button.defaultProps = {
  className: '',
  onClick: () => {},
};

Button.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
  text: PropTypes.string.isRequired,
};
