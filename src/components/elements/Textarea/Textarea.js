import React from 'react';
import styles from './styles.scoped.css';
import PropTypes from 'prop-types';

export default function Textarea({
  labelText,
  id,
  name,
  placeholder,
  onChange,
}) {
  return (
    <div className={styles.textarea}>
      <label htmlFor="textarea">{labelText}</label>
      <textarea
        id={id}
        name={name}
        onChange={onChange}
        placeholder={placeholder}
      />
    </div>
  );
}

Textarea.propTypes = {
  id: PropTypes.string.isRequired,
  labelText: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string.isRequired,
};
