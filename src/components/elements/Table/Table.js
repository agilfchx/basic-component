import React from 'react';
import PropTypes from 'prop-types';

export default function Table({ items, columns, rows }) {
  return (
    <table className="text-black bg-white w-full">
      <thead className="bg-[#F8FAFC] text-[#9CA3AF]">
        <tr>
          {columns.map((i, idx) => (
            <th className="text-left p-5" key={idx}>
              {i}
            </th>
          ))}
        </tr>
      </thead>
      <tbody>{items.map(rows)}</tbody>
    </table>
  );
}
Table.propTypes = {
  columns: PropTypes.array.isRequired,
  items: PropTypes.array.isRequired,
  rows: PropTypes.func.isRequired,
};
