import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import styles from './styles.scoped.css';
import Textarea from '../Textarea';
import Input from '../Input';
import Select from '../Select';
import Checkbox from '../Checkbox';
import Button from '../Button';
import Upload from '../Upload';

export default function Form() {
  const navigate = useNavigate();

  const [name, setName] = useState('');
  const [price, setPrice] = useState(0);
  const [description, setDescription] = useState('');
  const [category, setCategory] = useState('');
  const [checked, setChecked] = useState(false);
  const [expiryDate, setExpiryDate] = useState('');
  const [image, setImage] = useState('x');

  const onSubmit = () => {
    const newItem = {
      id: 'sku' + Math.floor(1000 + Math.random() * 9000),
      name,
      price,
      image,
      description,
      isDeleted: false,
      category,
      expiryDate: checked ? expiryDate : null,
    };
    const isEmpty = Object.values(newItem).every((i) => i !== '');
    if (!isEmpty) {
      alert('Please fill all the fields');
    } else {
      // console.log(newItem);
      return newItem;
    }
  };

  const handleSubmit = () => {
    const newItem = onSubmit();
    navigate('/products', { state: newItem });
  };

  const uploadImage = async (e) => {
    const file = e.target.files[0];
    const base64 = await convertBase64(file);
    setImage(base64);
  };

  const convertBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        resolve(reader.result);
      };
      reader.onerror = (error) => {
        reject(error);
      };
    });
  };

  return (
    <form action="">
      <div className={styles.form}>
        <div className={styles.form_group}>
          <div className={styles.row}>
            <Input
              id="name"
              labelText="Product name"
              name="name"
              onChange={(e) => setName(e.target.value)}
              placeholder="Enter product name"
              type="text"
            />
            <Input
              id="price"
              labelText="Product price"
              name="price"
              onChange={(e) => setPrice(e.target.value)}
              placeholder="Enter product price"
              type="number"
            />
            <Select
              id="category"
              name="category"
              onChange={(e) => setCategory(e.target.value)}
              option="Select category"
            >
              <label htmlFor="category">Product category</label>
            </Select>
          </div>
          <Textarea
            id="description"
            labelText="Product Description"
            name="description"
            onChange={(e) => setDescription(e.target.value)}
            placeholder="Enter product description"
          />
          <Checkbox
            className="checkbox-bottom"
            id="expiry"
            labelText="Product has expiry date"
            name="expiry"
            onChange={() => setChecked(!checked)}
            type="checkbox"
          />
          <div className={styles.input}>
            <label htmlFor={name}>Expiry Date</label>
            <input
              className={styles.input_box}
              disabled={!checked}
              id="expired"
              name="expired"
              onChange={(e) => setExpiryDate(e.target.value)}
              type="date"
            />
          </div>
        </div>
        <div className={styles.form_image}>
          <img alt="Image" className={styles.image} src={image} />
          <Upload onChange={(e) => uploadImage(e)} />
        </div>
      </div>
      <div className={styles.btn_field}>
        <Link to="/products">
          <Button text="Cancel" />
        </Link>
        <Button className="blue" onClick={handleSubmit} text="Save" />
      </div>
    </form>
  );
}
