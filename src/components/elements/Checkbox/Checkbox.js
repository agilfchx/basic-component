import React from 'react';
import './styles.css';
import PropTypes from 'prop-types';

export default function Checkbox({ labelText, id, name, className, onChange }) {
  return (
    <div className={`checkbox ${className}`}>
      <input
        className="checkbox-style"
        id={id}
        name={name}
        onChange={onChange}
        type="checkbox"
      />
      <label htmlFor={name}>{labelText}</label>
    </div>
  );
}

Checkbox.defaultProps = {
  className: '',
};

Checkbox.propTypes = {
  className: PropTypes.string,
  id: PropTypes.string.isRequired,
  labelText: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};
